package es.ua.eps.contentprovider;

import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class UsersActivity extends AppCompatActivity {

    List<User> users;
    UserSQLiteHelper helper;

    Spinner listUsersSpinner;
    UserAdapter adapter;
    int version;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_users);
        setTitle(getText(R.string.manage_users));

        listUsersSpinner = (Spinner) findViewById(R.id.spinner);

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        version = Integer.valueOf(preferences.getString("db_version", "1"));
        helper = new UserSQLiteHelper(this, preferences.getString("db_name", "users"), null, version);

        getUsers();

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                newUser();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        getUsers();
    }

    private void newUser() {
        Intent intent = new Intent(this, UserActivity.class);
        startActivity(intent);
    }

    public void onUpdate(View v) {
        Intent intent = new Intent(this, UserActivity.class);
        intent.putExtra(UserActivity.EXTRA_USER, (User) listUsersSpinner.getSelectedItem());
        startActivity(intent);
    }

    public void onDelete(View v) {
        String id = ((User)listUsersSpinner.getSelectedItem()).getId();
        getContentResolver().delete(Uri.withAppendedPath(UserProvider.CONTENT_URI, id), null, null);

        adapter = new UserAdapter(this, android.R.layout.simple_spinner_dropdown_item, users);
        listUsersSpinner.setAdapter(adapter);

        Toast.makeText(this, getText(R.string.deleted_successfully), Toast.LENGTH_SHORT).show();
    }

    private void getUsers() {
        Cursor c = getContentResolver().query(UserProvider.CONTENT_URI, null, null, null, null);

        users = new ArrayList<>();
        if (c.moveToFirst()) {
            do {
                users.add(UserSQLiteHelper.userFromCursor(c, version));
            } while (c.moveToNext());
        }

        adapter = new UserAdapter(this, android.R.layout.simple_spinner_dropdown_item, users);
        listUsersSpinner.setAdapter(adapter);
    }

    public void onBack(View v) {
        finish();
    }

}
