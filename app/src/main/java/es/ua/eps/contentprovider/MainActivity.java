package es.ua.eps.contentprovider;

import android.content.ContentResolver;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;


public class MainActivity extends AppCompatActivity {

    EditText username;
    EditText password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        username = (EditText) findViewById(R.id.login_name);
        password = (EditText) findViewById(R.id.login_password);
    }

    public void onLogin(View v) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        int version = Integer.valueOf(preferences.getString("db_version", "1"));

        if (username.getText().length() > 0 && password.getText().length() > 0) {
            Uri uri = Uri.withAppendedPath(UserProvider.CONTENT_URI, String.format("%s/%s", username.getText().toString(), password.getText().toString()));
            ContentResolver cr = getContentResolver();
            Cursor c = cr.query(uri, new String[]{}, null, null, null);

            if (c != null && c.moveToFirst()) {
                User u = UserSQLiteHelper.userFromCursor(c, version);
                Intent intent = new Intent(this, WelcomeActivity.class);
                intent.putExtra(WelcomeActivity.EXTRA_USER, u);
                startActivity(intent);
            } else {
                Toast.makeText(this, R.string.invalid_pass, Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(this, R.string.user_pass, Toast.LENGTH_SHORT).show();
        }
    }

    public void onExit(View v) {
        finish();
    }
}
