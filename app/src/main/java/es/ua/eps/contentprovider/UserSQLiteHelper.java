package es.ua.eps.contentprovider;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mastermoviles on 12/12/16.
 */
public class UserSQLiteHelper extends SQLiteOpenHelper {

    public static final String TABLE = "User";

    public static String sqlCreate = "CREATE TABLE " + TABLE + " (_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, name TEXT, username TEXT, password TEXT)";
    public static String sqlCreateV2 = "CREATE TABLE " + TABLE + " (_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, name TEXT, username TEXT, password TEXT, email TEXT)";
    public static String sqlAuthenticate = "SELECT * FROM " + TABLE + " WHERE username=? AND password=?";
    public static String sqlSelect = "SELECT * FROM " + TABLE + " WHERE _id=?";
    public static String sqlSelectAll = "SELECT * FROM " + TABLE;
    public static String sqlDrop = "DROP TABLE IF EXISTS " + TABLE;

    public UserSQLiteHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(sqlCreate);
        ContentValues values = new ContentValues();
        values.put("name", "Administrador");
        values.put("username", "admin");
        values.put("password", "admin");

        db.insert(TABLE, null, values);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        if (newVersion > 1) {

            List<User> users = getUsers(db);

            db.execSQL(sqlDrop);
            db.execSQL(sqlCreateV2);

            for (User u : users) {
                ContentValues uservalues = new ContentValues();
                uservalues.put("name", u.name);
                uservalues.put("username", u.username);
                uservalues.put("password", u.password);
                uservalues.put("email", "");

                db.insert(TABLE, null, uservalues);
            }
        }
    }

    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (newVersion <= 1) {
            List<User> users = getUsers(db);

            db.execSQL(sqlDrop);
            db.execSQL(sqlCreate);

            for (User u : users) {
                ContentValues uservalues = new ContentValues();
                uservalues.put("name", u.name);
                uservalues.put("username", u.username);
                uservalues.put("password", u.password);

                db.insert(TABLE, null, uservalues);
            }
        }
    }

    public List<User> getUsers(SQLiteDatabase db) {

        Cursor c = db.rawQuery(sqlSelectAll, null);
        List<User> users = new ArrayList<>();
        if (c.moveToFirst()) {
            do {
                users.add(userFromCursor(c, db.getVersion()));
            } while (c.moveToNext());
        }

        return users;
    }

    public static User userFromCursor(Cursor c, int version) {
        String code = c.getString(c.getColumnIndex("_id"));
        String name = c.getString(c.getColumnIndex("name"));
        String nick = c.getString(c.getColumnIndex("username"));
        String passwd = c.getString(c.getColumnIndex("password"));
        String email = "";
        if (version > 1) {
            email = c.getString(c.getColumnIndex("email"));
        }

        return new User(code, name, nick, passwd, email);
    }
}
