package es.ua.eps.contentprovider;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.SharedPreferences;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;

/**
 * Created by mastermoviles on 29/12/16.
 */
public class UserProvider extends ContentProvider {

    private static final String pkg = "es.ua.eps.contentprovider";

    // Content URI
    private static final String uri = "content://" + pkg + "/users";
    public static final Uri CONTENT_URI = Uri.parse(uri);

    // UriMatcher
    private static final UriMatcher uriMatcher;
    private static final int USERS = 1;
    private static final int USER_ID = 2;
    private static final int USER_LOGIN = 3;

    // Init URI Matcher
    static {
        uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        uriMatcher.addURI(pkg, "users", USERS);
        uriMatcher.addURI(pkg, "users/#", USER_ID);
        uriMatcher.addURI(pkg, "users/*/*", USER_LOGIN);
    }

    // MIME Types
    public static final String SINGLE_MIME = "vnd.android.cursor.item/vnd.basesqlite.User";
    public static final String MULTIPLE_MIME = "vnd.android.cursor.dir/vnd.basesqlite.User";

    // DB
    UserSQLiteHelper userSQLiteHelper;

    @Override
    public boolean onCreate() {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getContext());
        int version = Integer.valueOf(preferences.getString("db_version", "1"));
        userSQLiteHelper = new UserSQLiteHelper(getContext(), preferences.getString("db_name", "users"), null, version);

        return true;
    }

    @Nullable
    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        SQLiteDatabase db = userSQLiteHelper.getReadableDatabase();
        switch (uriMatcher.match(uri)) {
            case USERS:
                return db.rawQuery(UserSQLiteHelper.sqlSelectAll, null);
            case USER_ID:
                return db.rawQuery(UserSQLiteHelper.sqlSelect, new String[]{uri.getLastPathSegment()});
            case USER_LOGIN:
                String[] parts = uri.getEncodedPath().split("/");
                return db.rawQuery(UserSQLiteHelper.sqlAuthenticate, new String[]{parts[2], parts[3]});
            default:
                return null;
        }
    }

    @Nullable
    @Override
    public String getType(Uri uri) {
        switch (uriMatcher.match(uri)) {
            case USERS:
                return MULTIPLE_MIME;
            case USER_ID:
                return SINGLE_MIME;
            case USER_LOGIN:
                return SINGLE_MIME;
            default:
                return null;
        }
    }

    @Nullable
    @Override
    public Uri insert(Uri uri, ContentValues values) {
        SQLiteDatabase db = userSQLiteHelper.getWritableDatabase();
        switch (uriMatcher.match(uri)) {
            case USERS:
                long id = db.insert(UserSQLiteHelper.TABLE, null, values);
                return Uri.withAppendedPath(CONTENT_URI, String.valueOf(id));
            default:
                return null;
        }
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        SQLiteDatabase db = userSQLiteHelper.getWritableDatabase();
        switch (uriMatcher.match(uri)) {
            case USER_ID:
                String id = uri.getLastPathSegment();
                return db.delete(UserSQLiteHelper.TABLE, "_id = ?", new String[]{id});
            default:
                return 0;
        }
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        SQLiteDatabase db = userSQLiteHelper.getWritableDatabase();
        switch (uriMatcher.match(uri)) {
            case USER_ID:
                String id = uri.getLastPathSegment();
                return db.update(UserSQLiteHelper.TABLE, values, "_id = ?", new String[]{id});
            default:
                return 0;
        }
    }
}
