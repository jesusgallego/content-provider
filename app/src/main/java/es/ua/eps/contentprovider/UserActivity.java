package es.ua.eps.contentprovider;

import android.content.ContentValues;
import android.content.SharedPreferences;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class UserActivity extends AppCompatActivity {

    public static final String EXTRA_USER = "EXTRA_USER";

    User u;
    int version;

    EditText name;
    EditText username;
    EditText password;
    EditText email;
    Button actionButton;

    UserSQLiteHelper helper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user);

        setTitle(getText(R.string.manage_users));

        name = (EditText) findViewById(R.id.user_name);
        username = (EditText) findViewById(R.id.user_username);
        password = (EditText) findViewById(R.id.user_password);
        actionButton = (Button) findViewById(R.id.user_action_button);
        email = (EditText) findViewById(R.id.user_email);

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        version = Integer.valueOf(preferences.getString("db_version", "1"));

        if (version <= 1) {
            email.setVisibility(View.INVISIBLE);
            findViewById(R.id.textViewEmail).setVisibility(View.INVISIBLE);
        }

        helper = new UserSQLiteHelper(this, preferences.getString("db_name", "users"), null, version);

        u = (User) getIntent().getSerializableExtra(EXTRA_USER);
        if (u != null) {
            name.setText(u.name);
            username.setText(u.username);
            password.setText(u.password);

            actionButton.setText(getText(R.string.update));
            actionButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    updateUser();
                }
            });
        } else {
            u = new User();
            actionButton.setText(getText(R.string.create));
            actionButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    createUser();
                }
            });
        }
    }

    public void onBack(View v) {
        finish();
    }

    private void createUser() {
        u.setName(name.getText().toString());
        u.setUsername(username.getText().toString());
        u.setPassword(password.getText().toString());

        ContentValues values = new ContentValues();
        values.put("name", u.name);
        values.put("username", u.username);
        values.put("password", u.password);
        if (version > 1) {
            values.put("email", u.email);
        }

        getContentResolver().insert(UserProvider.CONTENT_URI, values);

        Toast.makeText(this, R.string.create_successfully, Toast.LENGTH_SHORT).show();
    }

    private void updateUser() {
        u.setName(name.getText().toString());
        u.setUsername(username.getText().toString());
        u.setPassword(password.getText().toString());
        if (version > 1) {
            u.setEmail(email.getText().toString());
        }

        ContentValues values = new ContentValues();
        values.put("name", u.name);
        values.put("username", u.username);
        values.put("password", u.password);
        if (version > 1) {
            values.put("email", u.email);
        }

        getContentResolver().update(Uri.withAppendedPath(UserProvider.CONTENT_URI, u.getId()), values, null, null);

        Toast.makeText(this, R.string.update_successfully, Toast.LENGTH_SHORT).show();
    }
}
